import csv

def csv_reader_classes(file):
  tupleArray = []

  with open(file, 'rt', encoding='utf8') as csv_file:
    reader = csv.reader(csv_file)

    for i in reader:
        tupleArray.append(tuple(i))

  return tupleArray