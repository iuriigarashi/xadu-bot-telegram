import sqlite3
from csv_reader import csv_reader_classes

#creates the db file
conn = sqlite3.connect('db/xadu_bot.db')

c = conn.cursor()

#create table of classes
#uncomment if the file xadu_bot.db doesn't exist.
c.execute('''CREATE TABLE classes (id text primary_key, classname text, classroom text, schedule text)''')

#read the classes.csv and put in a tuple
tupledata = csv_reader_classes('doc/classes_db.txt')
print(tupledata)
print('\n\n\n')

#store the tuple in the db
c.executemany('INSERT INTO classes VALUES (?,?,?,?)', tupledata)

#see the data in classes table
c.execute('SELECT * from classes')
print(c.fetchall())

#commit the changes and closes the connection
conn.commit()
conn.close()