import telepot
import random
from agenda import is_asking_classes
from agenda import stringify_agenda
from agenda import open_csv_classes
from randcats import sendcat


greeting_texts = ['dae', 'bom', 'olá', 'boa', 'dia',
                  'tarde', 'noite', 'ohayou', 'oi', 'oie']
greeting_send = ['Dae', 'Olá', 'Ohayou', 'Oi', 'Oie', 'Oieeee']


def is_greeting_message(msg):
    received_text = msg['text'].lower()
    words_received = received_text.split(' ')
    result = False
    texts = []
    for i in words_received:
        for j in greeting_texts:
            if i == j:
                result = True
                texts.append(j)
    return [result, texts]


def chat_routes(xadu_bot, msg):
    _id = msg['from']['id']
    name = msg['from']['first_name']

    #print(msg)

    is_greeting = is_greeting_message(msg)

    #commands goes here
    if msg['text'] == '/start':
        send_text = 'Oi, Eu sou o <b>xadu</b>! Um bot que te ajudará durante a jornada de BSI com lembretes e informações sobre suas aulas e compromissos. Tenho uma série de comandos pré-definidos e consigo compreender alguma coisa ou outra básica derivada desses comandos.\n\nNão fale coisas muito difíceis senão infelizmente não vou entender, pois sou meio burrinho.😥\n\nMas chega de falar de mim, vamos falar sobre você, ' + \
            name + '! Use algum dos meus comandos digitando "/" ou escreva algo pra que eu possa lhe ajudar.😊'
    elif is_greeting[0]:
        greeting = greeting_send[random.randint(0, greeting_send.__len__())]
        send_text = greeting + " meu caro " + name + "!"
    elif msg['text'].find('/aulas') != -1:
        is_classes = is_asking_classes(msg)
        send_text = stringify_agenda(is_classes[0], is_classes[1])
    elif msg['text'].find('/cat') != -1:
        sendcat(xadu_bot, msg)
        send_text = 'Um gatinho bonitinho vindo de 🐱thecatapi.com🐱' 
    # Default error message
    else:
        randimg = ['assets/nao_entendi_01.jpg', 'assets/nao_entendi_02.jpg']
        xadu_bot.sendPhoto(msg['from']['id'], open(
            randimg[random.randint(0, 1)], 'rb'))
        send_text = "Como assim? Não entendi."

    return send_text
