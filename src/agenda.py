
import telepot
import random
import csv
import sqlite3
from datetime import datetime

is_asking_classes_text = ['/aula', '/aulas', 'aula', 'aulas']


def open_csv_classes(file):
    values = []
    tuplelist = []

    with open(file, 'rt', encoding='utf8') as csv_file:
        reader = csv.reader(csv_file)

        for i in reader:
            for j in i:
                if(j.split(',').__len__() > 1):
                    j = j.split(',')
                    values.append(j)
            tuplelist.append((i[0], values))
            values = []

    dictclasses = dict(tuplelist)
    return dictclasses


def tuple_to_classes_dict(data):
    seg = ['seg', []]
    ter = ['ter', []]
    qua = ['qua', []]
    qui = ['qui', []]
    sex = ['sex', []]
    sab = ['sab', []]
    dom = ['dom', []]
    schedule = []
    for i in data:
      s = i[3].split(';')
      #separates the schedule for that class
      for j in s:
        j = j.split(',')
        schedule.append(j)
      for d in schedule:
        classarray = [i[1], i[2], d[1], d[2]]
        print(classarray)

        if(d[0] == 'seg'):
          seg[1].append(classarray)
        elif(d[0] == 'ter'):
          ter[1].append(classarray)
        elif(d[0] == 'qua'):
          qua[1].append(classarray)
        elif(d[0] == 'qui'):
          qui[1].append(classarray)
        elif(d[0] == 'sex'):
          sex[1].append(classarray)
        elif(d[0] == 'sab'):
          sab[1].append(classarray)
        elif(d[0] == 'dom'):
          dom[1].append(classarray)
      schedule = []
    #transform the day of weeks into tuples
    seg = tuple(seg)
    ter = tuple(ter)
    qua = tuple(qua)
    qui = tuple(qui)
    sex = tuple(sex)
    sab = tuple(sab)
    dom = tuple(dom)
    return dict([seg,ter,qua,qui,sex,sab,dom])


#connect to db
conn = sqlite3.connect('db/xadu_bot.db')

#select the classes from db
c = conn.cursor()
c.execute('SELECT * from classes')
classes = c.fetchall()

#classes_xadu = open_csv_classes('doc/classes.txt')
classes_xadu = tuple_to_classes_dict(classes)


def is_asking_classes(msg):
    received_text = msg['text'].lower()
    words_received = received_text.split(' ')
    """ result = False
    for i in words_received:
        for j in is_asking_classes_text:
            if i == j:
                result = True """
    
    # search for the day : default - today
    day = datetime.now().isoweekday()
    for i in words_received:
      if i == 'seg' or i == 'segunda':
        day = 1
        dayrequest = 'segunda-feira'
      elif i == 'ter' or i == 'terça':
        day = 2
        dayrequest = 'terça-feira'
      elif i == 'qua' or i == 'quarta':
        day = 3
        dayrequest = 'quarta-feira'
      elif i == 'qui' or i == 'quinta':
        day = 4
        dayrequest = 'quinta-feira'
      elif i == 'sex' or i == 'sexta':
        day = 5
        dayrequest = 'sexta-feira'
      elif i == 'sab' or i == 'sábado':
        day = 6
        dayrequest = 'sábado'
      elif i == 'dom' or i == 'domingo':
        day = 7
        dayrequest = 'domingo'
      elif i == 'amanhã':
        day = day+1
        if(day == 8):
          day = 1
        dayrequest = 'amanhã'
      elif i == 'ontem':
        day = day-1
        if(day == 0):
          day = 7
        dayrequest = 'ontem'
      else:
        dayrequest = 'hoje'

    

    # translates the day int into string
    if(day == 1):
        dayoftheweek = 'seg'
    elif(day == 2):
        dayoftheweek = 'ter'
    elif(day == 3):
        dayoftheweek = 'qua'
    elif(day == 4):
        dayoftheweek = 'qui'
    elif(day == 5):
        dayoftheweek = 'sex'
    elif(day == 6):
        dayoftheweek = 'sab'
    else:
        dayoftheweek = 'dom'

    # Inform the classes that xadu has in that day
    classes = classes_xadu[dayoftheweek]

    return [dayrequest, classes]


def stringify_agenda(dayrequest, classes):
    if not classes:
        return 'Você não tem aulas marcadas para ' + dayrequest + '.😁'
    else:
        s = '📓Você tem as seguintes aulas ' + dayrequest + ':\n\n'
        for i in classes:
            s += '<b>' + i[0] + '</b> : ' + '\n        <i>-horário: ' + i[2] + \
                '-' + i[3] + '</i>' + '\n        <i>-sala: ' + \
                i[1] + '</i>' + '\n'
        return s
