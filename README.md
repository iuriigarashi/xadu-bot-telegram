## Xadu Bot Telegram

Bot criado para testar as funcionalidades do Telegram Bot. Programado por Iuri Igarashi.

## Comandos do bot

- **/start** - saudação do bot, onde ele diz suas principais funcionalidades
- **/aulas :diadasemana** - mostra as aulas de um dia especificado pelo usuário
    - **opções de _:diadasemana_** - seg, ter, qua, qui, sex, sab, dom
    - caso não especificado ou não entenda, ele pega o dia atual.

## O que ele faz?

- Manda Mensagem de bom dia
- Lista as aulas que eu tenho em um dia determinado
- Manda meme do pai de família(como assim, não entendi?) quando ele não reconhece o comando dado pelo input do usuário

## Tecnologias utilizadas

- sqlite3
- python 3.7.4
- telepot
